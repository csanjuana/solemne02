/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.entity.Nuevoalumno;

/**
 *
 * @author csanjuana
 */
public class NuevoalumnoJpaController implements Serializable {

    public NuevoalumnoJpaController() {
       
    }
     private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");	

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Nuevoalumno nuevoalumno) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(nuevoalumno);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNuevoalumno(nuevoalumno.getRut()) != null) {
                throw new PreexistingEntityException("Nuevoalumno " + nuevoalumno + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Nuevoalumno nuevoalumno) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            nuevoalumno = em.merge(nuevoalumno);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = nuevoalumno.getRut();
                if (findNuevoalumno(id) == null) {
                    throw new NonexistentEntityException("The nuevoalumno with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Nuevoalumno nuevoalumno;
            try {
                nuevoalumno = em.getReference(Nuevoalumno.class, id);
                nuevoalumno.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The nuevoalumno with id " + id + " no longer exists.", enfe);
            }
            em.remove(nuevoalumno);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Nuevoalumno> findNuevoalumnoEntities() {
        return findNuevoalumnoEntities(true, -1, -1);
    }

    public List<Nuevoalumno> findNuevoalumnoEntities(int maxResults, int firstResult) {
        return findNuevoalumnoEntities(false, maxResults, firstResult);
    }

    private List<Nuevoalumno> findNuevoalumnoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Nuevoalumno.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Nuevoalumno findNuevoalumno(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Nuevoalumno.class, id);
        } finally {
            em.close();
        }
    }

    public int getNuevoalumnoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Nuevoalumno> rt = cq.from(Nuevoalumno.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
