<%-- 
    Document   : detalle_alumno
    Created on : 17-06-2021, 0:29:45
    Author     : csanjuana
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.entity.Nuevoalumno"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Nuevoalumno> lista = (List<Nuevoalumno>) request.getAttribute("listaAlumnos");
    Iterator<Nuevoalumno> itNuevoalumno = lista.iterator();
%>     
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alumnos inscritos</title>
    </head>
    <body>

        <form  name="form" action="NuevoAlumnoController" method="POST">



            <table border="1">
                <thead>
                <th>Rut</th>
                <th>Nombre</th>

                <th>Correo</th>
                <th>Telefono</th>
                <th>Sede</th>
                </thead>
                <tbody>
                    <%while (itNuevoalumno.hasNext()) {
                                        Nuevoalumno Alumno = itNuevoalumno.next();%>
                    <tr>
                        <td><%= Alumno.getRut()%></td>
                        <td><%= Alumno.getNombre()%></td>
                        <td><%= Alumno.getCorreo()%></td>
                        <td><%= Alumno.getTelefono()%></td>
                        <td><%= Alumno.getJornada()%></td>


                    </tr>
                    <%}%>                
                </tbody>           
            </table>

        </form>               



    </body>
</html>
