<%-- 
    Document   : mantenedor_alumnos
    Created on : 18-06-2021, 1:02:14
    Author     : csanjuana
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.entity.Nuevoalumno"%>
<%@page import="root.entity.Nuevoalumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Nuevoalumno> lista = (List<Nuevoalumno>) request.getAttribute("listaAlumnos");
    Iterator<Nuevoalumno> itNuevoalumno = lista.iterator();
%>   

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <form  name="form" action="MantenerAlumnos" method="POST">



            <table border="1">
                <thead>
                <th>Rut</th>
                <th>Nombre</th>

                <th>Correo</th>
                <th>Telefono</th>
                <th>Sede</th>
                </thead>
                <tbody>
                    <%while (itNuevoalumno.hasNext()) {
                            Nuevoalumno Alumno = itNuevoalumno.next();%>
                    <tr>
                        <td><%= Alumno.getRut()%></td>
                        <td><%= Alumno.getNombre()%></td>
                        <td><%= Alumno.getCorreo()%></td>
                        <td><%= Alumno.getTelefono()%></td>
                        <td><%= Alumno.getJornada()%></td>
                        <td> <input type="radio" name="seleccion" value="<%= Alumno.getRut()%>"> </td>


                    </tr>
                    <%}%>                
                </tbody>           
            </table>

            <button type="submit" name="accion" value="agregar" class="btn btn-success">Agregar</button>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">eliminar</button>
            <button type="submit" name="accion" value="actualizar" class="btn btn-success">actualizar</button>


        </form>  




    </body>
</html>
