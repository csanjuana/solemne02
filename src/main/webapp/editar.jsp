<%-- 
    Document   : index
    Created on : 13-06-2021, 0:01:04
    Author     : csanjuana
--%>

<%@page import="root.entity.Nuevoalumno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
  Nuevoalumno alumno=(Nuevoalumno)request.getAttribute("al");
 
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edición de alumno</title>
    </head>
    <body>
        <p style="text-align: left;">Formulario renovaci&oacute;n pase escolar</p>
        <form action="MantenerAlumnos" method="POST" name="form">
            <table style="border-collapse: collapse; width: 48.7329%; height: 90px;" border="0">
                <tbody>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Rut</td>
                        <td style="width: 50%; height: 18px;"><input id="rut" name="rut" value="<%= alumno.getRut() %>"
 type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Nombre</td>
                        <td style="width: 50%; height: 18px;"><input id="nombre" name="nombre" value="<%= alumno.getNombre()%>" type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Correo</td>
                        <td style="width: 50%; height: 18px;"><input id="correo" name="correo" value="<%= alumno.getCorreo()%>" type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Telefono</td>
                        <td style="width: 50%; height: 18px;"><input id="telefono" name="telefono" value="<%= alumno.getTelefono()%>" type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Jornada</td>
                        <td style="width: 50%; height: 18px;"><input id="jornada" name="jornada" value="<%= alumno.getJornada()%>" type="text" /></td>
                    </tr>
                </tbody>
            </table>
            <br /><br /><button class="btn btn-success" name="accion" type="submit" value="grabar">Ingreso Solicitud</button></form>
            
            
    </body>
</html>
