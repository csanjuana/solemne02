<%-- 
    Document   : index
    Created on : 13-06-2021, 0:01:04
    Author     : csanjuana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulario pase escolar</title>
    </head>
    <body>
        <p style="text-align: left;">Formulario renovaci&oacute;n pase escolar</p>
        <form action="NuevoAlumnoController" method="POST" name="form">
            <table style="border-collapse: collapse; width: 48.7329%; height: 90px;" border="0">
                <tbody>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Rut</td>
                        <td style="width: 50%; height: 18px;"><input id="rut" name="rut" type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Nombre</td>
                        <td style="width: 50%; height: 18px;"><input id="nombre" name="nombre" type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Correo</td>
                        <td style="width: 50%; height: 18px;"><input id="correo" name="correo" type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Telefono</td>
                        <td style="width: 50%; height: 18px;"><input id="telefono" name="telefono" type="text" /></td>
                    </tr>
                    <tr style="height: 18px;">
                        <td style="width: 50%; height: 18px;">Jornada</td>
                        <td style="width: 50%; height: 18px;"><input id="jornada" name="jornada" type="text" /></td>
                    </tr>
                </tbody>
            </table>
            <br /><br /><button class="btn btn-success" name="accion" type="submit" value="Ingreso">Ingreso Solicitud</button></form>
            
             
    </body>
</html>
